# Bookmarker

*Gagnez en productivité, épargnez la planète. Avec Bookmarker, personnalisez et regroupez instantanément vos bookies\* par dossier, ciblez rapidement vos adresses préférées ou partez à l'assaut du web grâce aux moteurs de recherche.*

\* Bookie = marque-page, favori.

## Pourquoi utiliser un gestionnaire de favoris ?

En tant qu'utilisateurs du Web, nous avons le devoir de maitriser notre activité et d'en limiter les impacts environnementaux. Un bookie est semblable à un trajet mémorisé vers une page web déjà découverte ; il permet d'y accéder à nouveau sans avoir à reconsulter un moteur de recherche, ce qui serait chronophage et inutilement coûteux en requêtes serveur.

Éliminer les intermédiaires grâce aux bookies permet de :
* Diviser de manière significative l'empreinte énergétique de son activité en ligne. 
* Gagner en temps dans sa navigation web au quotidien.
* Limiter la collecte de ses données personnelles.

## Installer Bookmarker sur Firefox

Bookmarker est une WebExtension. Pour l'installer, rendez-vous sur la page Bookmarker du catalogue d'extensions de votre navigateur web et suivez la procédure décrite à l'écran.

## Mode d'emploi

### Retrouver un bookie

Saisissez quelques caractères dans le champ de recherche pour isoler les bookies dont l'intitulé correspond à votre recherche, sélectionnez un dossier depuis le menu déroulant pour restreindre l'affichage aux bookies contenus dans ce seul dossier.

### Ouvrir un ou plusieurs bookies

Cliquez sur un bookie pour l'ouvrir dans un nouvel onglet ou appuyer sur ```Entrée``` depuis le champ de recherche pour ouvrir tous les bookies visibles dans des onglets séparés. **Prudence : cette action est susceptible d'ouvrir beaucoup d'onglets !**

### Rechercher sur le web

Saisissez votre requête dans le champ de recherche puis sélectionnez Brave, DuckDuckGo ou Google pour rechercher sur le web. En l'absence de bookies, pressez simplement ```Entrée``` depuis le champ de recherche pour utiliser Brave, défini par défaut. Ces moteurs de recherche ont chacun leurs particularités ; les résultats qu'ils proposent sont également différents. A vous d'utiliser celui qui répond le mieux à vos attentes.

### Astuces

Naviguez au clavier pour gagner du temps !

* ```Alt``` + ```b``` depuis Firefox pour ouvrir Bookmarker. Le focus est immédiatement donné au champ de recherche.
* ```Tab``` pour progresser à travers les moteurs de recherche puis à travers les bookies.
* ```Shift``` + ```Tab``` pour revenir en arrière.
* ```Entrée``` pour activer l'élément ayant le focus.
				
## Données personnelles

C'est bien simple : Bookmarker ne collecte aucune donnée.