"use strict";

const bookiesRootDirId = 'toolbar_____';

class Model {

	static getBookies() {
		return new Promise(
			(res) => {
				let data = {
					bookies: [],
					directories: [], // TODO: fully replace this array by the 2 following
					dirFromIdToName : {},
					dirFromNameToId : {}
				}
				browser.bookmarks.getSubTree(bookiesRootDirId)
					.then(bookiesTree => res(
						(function extract(elm, arr, parents = ''){
							// Only directories that are not empty
							if(elm.type === 'folder') {
								if(elm.children.length > 0) {
									if(bookiesRootDirId !== elm.id) {
										data.directories.push({
											id: elm.id,
											title: elm.title
										});
										data.dirFromIdToName[elm.id] = elm.title;
										data.dirFromNameToId[elm.title] = elm.id;
									}
									for(const child of elm.children) {
										extract(child, arr, parents !== '' ?
											parents + '|||' + elm.title:
											elm.title
										);
									}
								}
							}
							// Only bookmarks
							else {
								elm.parents = parents.split('|||');
								elm.parents.shift();
								arr.push(elm);
							}
							return data;
						})(bookiesTree[0], data.bookies)
					)
				);
			}
		)
	}

	static getSettings(data) {
		return new Promise((res) => {		
			browser.storage.local.get(null)
				.then(resp => {
					data.settings = resp.settings;
					res(data);
			});
		});
	}

	static saveSettings(data) {
		return new Promise(
			function(res) {
				browser.storage.local.set({ settings: data.settings })
					.then(() => res(data)); 
			}
		);
	}

	static initSettings(data) {
		return new Promise((res) => {
			// data.directories is about directories existing on runtime.
			// data.settings is about directories saved on local storage.
			if(data.settings === undefined) {
				data.settings = [];
			}
			// Adds missing sets.
			let isSetDefined;
			for(let i = 0, l = data.directories.length; i < l; i++) {
				isSetDefined = false;
				for(let j = 0, k = data.settings.length; j < k; j++) {
					if(data.settings[j].dirId === data.directories[i].id) {
						isSetDefined = true;
						break;
					}
				}
				if(!isSetDefined) {
					data.settings.push(
						{
							dirId: data.directories[i].id,
							bgColor: 'ffffff',
							fontColor: '555555'
						}
					);
				}
			}
			// Removes useless sets.
			let isSetRelevant;
			for(let i = 0, l = data.settings.length; i < l; i++) {
				isSetRelevant = false;
				for(let j = 0, k = data.directories.length; j < k; j++) {
					if(data.settings[i].dirId === data.directories[j].id) {
						isSetRelevant = true;
						break;
					}
				}
				if(!isSetRelevant) {
					data.settings.splice(i, 1);
					i--;
					l--;
				}
			}
			res(data);
		});
	}

	static submitQuery(search, engine) {
		// Engine sent: uses this one.
		if(engine) {
			let searchURL;
			switch(engine) {
				case 'brave':
					searchURL = 'https://search.brave.com/search?q=';
					break;
				case 'duckduckgo':
					searchURL = 'https://duckduckgo.com/?q=';
					break;
				case 'google':
					searchURL = 'https://www.google.fr/search?q=';
					break;
			}
			browser.tabs.create({ url: searchURL + search });
		}
		// Engine not sent AND there are active bookies: opens ALL the visible bookies.
		else if(!engine && document.getElementsByClassName('visible').length > 0) {
			const bookieElms = document.getElementsByClassName('visible');
			const bookieElmsLength = bookieElms.length;
			if(bookieElmsLength > 1) {
				if(confirm(`Souhaitez-vous vraiment ouvrir ${bookieElmsLength} bookies ?`)) {
					for(let i = 0, l = bookieElmsLength; i < l; i++) {
						browser.tabs.create({url: bookieElms[i].getAttribute('data-href') });
					}
				}
			}
			else {
				browser.tabs.create({url: bookieElms[0].getAttribute('data-href') });
			}
		}
		// Engine not sent AND there are NO active bookies: opens the search results into a new tab.
		else {
			Model.submitQuery(search, 'brave');
		}
		window.close();
	}

};

class View {
	
	static setCounter(count = 0) {
		document.querySelector('.bookies__counter').textContent = `${count} bookie(s)`;
	}

	static setSelect(optionID) {
		document.querySelector(`option[value='${optionID}`).selected = true;
	}

	static undisplayElm(elm) {
		elm.classList.add('--hidden');
	}

	static displayElm(elm) {
		elm.classList.remove('--hidden');
	}

	static undisplayElmBySelector(selector) {
		document.querySelector(selector).classList.add('--hidden');
	}
	
	static displayElmBySelector(selector) {
		document.querySelector(selector).classList.remove('--hidden');
	}

	static buildBookies(data) {
		for(const bookie of data.bookies) {
			// Bookie container
			let bookieElm = document.createElement('div');
			bookieElm.classList.add(bookie.parentId, 'bookie', 'visible');
			bookieElm.setAttribute('data-href', bookie.url);
			bookieElm.setAttribute('data-parents', bookie.parents);	
			bookieElm.id = bookie.title;
			// Bookie parents
			let bookieParentsElm = document.createElement('div');
			bookieParentsElm.classList.add('bookie__parents');
			for(const parent of bookie.parents) {
				let separatorElm = document.createElement('span');
				separatorElm.textContent = '/';
				let bookieParentAElm = document.createElement('a');
				bookieParentAElm.classList.add('bookie__parents__a');
				bookieParentAElm.textContent = parent;
				bookieParentAElm.onclick = (e) => {
					const parentId = data.dirFromNameToId[parent];
					View.filterBookies('', data.dirFromIdToName[parentId]);
					View.setSelect(parentId);
					e.preventDefault();
				}
				bookieParentsElm.appendChild(separatorElm);
				bookieParentsElm.appendChild(bookieParentAElm);
			}
			// Bookie main hyperlink
			let bookieAElm = document.createElement('a');
			bookieAElm.classList.add('bookie__a');
			bookieAElm.textContent = bookie.title;
			bookieAElm.href = bookie.url;
			bookieAElm.setAttribute('target', '_blank');
			// Appending
			bookieElm.appendChild(bookieParentsElm);
			bookieElm.appendChild(bookieAElm);
			document.querySelector('.bookies__container').appendChild(bookieElm);
		}
	}

	// Useful to avoid code duplication with the "buildSelect" method
	static buildOption(option) {
		let optionElm = document.createElement('option');
		optionElm.textContent = option.label;
		optionElm.value = option.value;
		if(option.selected) {
			optionElm.selected = true;
		}
		return optionElm;
	}

	// Where user can navigate between its bookies directories
	static buildSelect(data) {
		const selectElm = document.querySelector('.bookies__cat-form__sel');
		View.undisplayElm(selectElm);		
		// Default option
		selectElm.appendChild(
			View.buildOption({ 
				label: 'Tout montrer',
				value: 'default',
				selected: true
			})
		);
		// Custom options based on existing directories
		for(const directory of data.directories) {
			selectElm.appendChild(
				View.buildOption({ 
					label: directory.title,
					value: directory.id,
					selected: false
				})
			);
		}
		View.displayElm(selectElm);
	}

	static buildSettings(data) {
		let HTMLTemplate = '';	
		for(let i = 0, l = data.settings.length; i < l; i++) {
			let dirTitle = '';
			for(const directory of data.directories) {
				if(directory.id === data.settings[i].dirId) {
					dirTitle = directory.title;
				}
			}
			HTMLTemplate += `
				<fieldset class="set">
					<legend class="set__legend" data-dir-id="${data.settings[i].dirId}">${dirTitle}</legend>
					<label>
						Police d'écriture
						<input 
							class="set__parameter set-font"
							data-css-property="color"
							pattern="[0-9a-fA-F]{6}"
							maxlength="6" 
							minlength="3"
							type="text" 
							value="${data.settings[i].fontColor}" 
							required />
					</label>
					<label>
						Arrière plan
						<input 
							class="set__parameter set-background"
							data-css-property="backgroundColor"
							pattern="[0-9a-fA-F]{6}" 
							maxlength="6"
							minlength="3"
							type="text" 
							value="${data.settings[i].bgColor}" 
							required />
					</label>
				</fieldset>
			`;											
		}
		document.querySelector('.settings__sets').innerHTML = HTMLTemplate;
	}

	static applySettings(data) {
		View.undisplayElmBySelector('.bookies__container');
		for(let i = 0, l = data.settings.length; i < l; i++) {
			let bookieElms = document.getElementsByClassName(data.settings[i].dirId);
			for(let j = 0, k = bookieElms.length; j < k; j++) {
				bookieElms[j].style.color = '#' + data.settings[i].fontColor;
				bookieElms[j].style.backgroundColor = '#' + data.settings[i].bgColor;	
			}
		}
		View.displayElmBySelector('.bookies__container');
	}
	
	static filterBookies(userInput, directoryName) {
		View.undisplayElmBySelector('.bookies__container');
		// First, hide all bookies
		const bookieElms = document.getElementsByClassName('bookie');
		for(const bookieElm of bookieElms) {
			bookieElm.classList.remove('visible');
			bookieElm.classList.add('--hidden');	
		}
		const filteredElms = [];
		// Then, only target bookies matching with the selected category and its known parents
		if(directoryName !== undefined) { // Equal to 'Tout montrer'
			for(const bookieElm of bookieElms) {
				const bookieParents = bookieElm.getAttribute('data-parents');
				if(bookieParents.includes(directoryName)) {
					filteredElms.push(bookieElm);
				}
			}
		}
		// Or target all bookies
		else {
			for(const bookieElm of bookieElms) {
				filteredElms.push(bookieElm);
				
			}
		}
		// Finally, show selection that also matches with the userInput 
		for(const filteredElm of filteredElms) {
			if(filteredElm.id.toLowerCase().indexOf(userInput.toLowerCase()) >= 0) {
				filteredElm.classList.remove('--hidden');
				filteredElm.classList.add('visible');
			}
		}
		View.setCounter(document.getElementsByClassName('visible').length); 
		View.displayElmBySelector('.bookies__container', true);	
	}
	
};

Model.getBookies()
	.then(data => Model.getSettings(data)
		.then(data => Model.initSettings(data)
			.then(data => Model.saveSettings(data)
				.then(data => {
					data.bookies.sort((a, b) => {
						return a.title > b.title;
					});
					data.directories.sort((a, b) => {
						return a.title > b.title;
					});
					/* ### TEMPLATING ### */
					View.undisplayElmBySelector('section.bookies');
					View.undisplayElmBySelector('section.settings');
					View.buildBookies(data);
					View.buildSelect(data);
					View.buildSettings(data);
					View.applySettings(data); 
					/* ### EVENTS ### */
					const searchInputElm = document.querySelector('input[type=text]');
					const searchSelectElm = document.querySelector('.bookies__cat-form__sel');
					// Searches matches in specific directory (and its subfolders).
					searchInputElm.oninput = () => {
						View.filterBookies(
							searchInputElm.value,  
							data.dirFromIdToName[searchSelectElm.value]
						); 	
					}
					// Limits visibility to a specific directory.
					document.querySelector('.bookies__cat-form').onchange = () => {
						View.filterBookies(
							searchInputElm.value, 
							data.dirFromIdToName[searchSelectElm.value]
						);
					}
					// Submits a query to the selected search engine.
					for(const engine of document.getElementsByClassName('engine')) {
						engine.onclick = e => {
							Model.submitQuery(searchInputElm.value, engine.id);
							e.preventDefault();
						};
					}
					// Opens the visible bookie(s) in new tab(s).
					document.querySelector('.bookies form').onsubmit = e => {
						Model.submitQuery(searchInputElm.value);
						e.preventDefault();
					};
					// Opens settings panel.
					document.querySelector('.settings__open-bt').onclick = () => {
						View.undisplayElmBySelector('section.bookies');
						View.displayElmBySelector('section.settings');
					}
					// Closes settings panel without saving.
					document.querySelector('.settings__close-bt').onclick = () => {
						View.undisplayElmBySelector('section.settings');
						View.displayElmBySelector('section.bookies');
					}
					// Updates live previews in the settings panel.
					for(const elm of document.getElementsByClassName('set__parameter')) {
						const cssProperty = elm.getAttribute('data-css-property');
						elm.closest('fieldset').style[cssProperty] = '#' + elm.value;
						elm.oninput = () => elm.closest('fieldset').style[cssProperty] = '#' + elm.value;
					}
					// Closes settings panel and updates changes.
					document.querySelector('.settings__save-bt').onclick = () => {
						data.settings = [];
						for(const setElm of document.getElementsByClassName('set')) {
							data.settings.push(
								{
									dirId: setElm.querySelector('.set__legend').getAttribute('data-dir-id'),
									bgColor: setElm.querySelector('.set-background').value,
									fontColor: setElm.querySelector('.set-font').value
								}
							);
						}
						Model.saveSettings(data).then(data => {
							View.applySettings(data)
							View.undisplayElmBySelector('section.settings');
							View.displayElmBySelector('section.bookies');
						});
					};
					View.setCounter(data.bookies.length);
					View.displayElmBySelector('section.bookies');
				}
			)
		)
	)
);